# EMBEDDED SYSTEMS

Embedded systems are found in many devices in common use today. The capabilities provided by embedded systems enable electronic equipment to have far greater capabilities than would be possible if only hardware techniques were used. As a result, embedded systems are found in all manner of electronic equipment and gadgets.

Below are the basic electronic devices used in embedded systems.

# SENSORS & ACTUATORS

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/0*NIzIJMOgy6Aich2m.png)


| Sensors  | Actuators |
| ------   | ------    |
| Converts the phyical gesture into electrical signals |  Converts electrical signal to mechanical work         |
| Takes the input from environment and gives to the system by converting it | Takes the input from the system and gives output to the environment.|

### Few Sensors
- Water Level IoT sensor
- Temperature IoT sensor
- Light IoT sensor
- Pressure,Proximity or Motion IoT sensor
- Chemical IoT sensor
- Image IoT sensor etc.

### Few Actuators
- Linear Actuators
- Motor 
- Relays
- Solenoids

![sensor and acutator](https://bridgera.com//wp-content/uploads/2017/06/sensor_actuator_graphicssec1_pg16.jpg)

# ANALOG AND DIGITAL

Analog and digital signals are different types which are mainly used to carry the data from one apparatus to another. Analog signals are continuous wave signals that change with time period whereas digital is a discrete signal is a nature. The main difference between analog and digital signals is, analog signals are represented with the sine waves whereas digital signals are represented with square waves. Let us discuss some dissimilarity of analog & digital signals.

![](https://cdn1.byjus.com/wp-content/uploads/2018/11/physics/2016/11/25112340/Analog-and-Digital.png)

Below is the diference between the analog and digital signals.

| Analog Signals  | Digital Signals |
| ------   | ------    |
| Continuous signals | Discrete signals |
| Represented by sine waves | Represented by square waves |
| Human voice, natural sound, analog electronic devices are few examples | Computers, optical drives, and other electronic devices |
| Continuous range of values | Discontinuous values |
| Only be used in analog devices | Suited for digital electronics like computers, mobiles and more |

# Microprocessors and Microcontrollers

| Microprocessors | Microcontrollers |
| ------ | ------ |
| It is the heart of Computer system | It is the heart of an embedded system |
| Consists of only a Central Processing Unit| Contains a CPU, Memory, I/O all integrated into one chip |
| Uses an external bus to interface to RAM, ROM, and other peripherals| Uses an internal controlling bus |
| Based on Von Neumann architecture| Based on Harvard architecture|
| It's complex and expensive, with a large number of instructions to process.| It's simple and inexpensive with less number of instructions to process.|
| It has a smaller number of registers, so more operations are memory-based| It has more register. Hence the programs are easier to write. |

### When to use

For small applications where only few basic task to be done ,a microcontroller can be used as it comes with all basic components such as RAM,ROM etc in buit on it. Microprocessor has powerful configuration than a Microcontroller.

![archi](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)

# Raspberry Pi 

The Raspberry Pi is a low cost, small sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.

### Pin Diagram

![RPI](https://www.electronicwings.com/public/images/user_images/images/Raspberry%20Pi/RaspberryPi_GPIO/Raspberry%20pi%203%20GPIO_pins_v2.png)

### Features

* CPU: Quad-core 64-bit ARM Cortex A53 clocked at 1.2 GHz
* Clock: 400MHz
* RAM: 1GB 
* USB ports: 4
* Peripherals: 17 GPIO plus specific functions, and HAT ID bus
* Bluetooth: 4.1
* Power source: 5 V 
* Network: 10/100Mbps Ethernet and 802.11n Wireless LAN

### Raspberry PI interfaces

* GPIO
* UART
* SPI
* I2C

# Communication

* The Communication inside computers take place in two ways -
    * Serial
    * Parallel

![](https://techdifferences.com/wp-content/uploads/2016/04/Untitled.jpg)

|Basis | Serial | Parallel |
|-----| ------ | ------ |
|Data transmission speed|Slow|Comparatively fast|
|Suitable for| Long Distance|Short Distance|
|High frequency operation| More Efficient | less Efficient|
|Number of transmitted bit/clock cycle| Only one bit| Many bits|
|Examples|UART,SPI,I2C|GPIO|



