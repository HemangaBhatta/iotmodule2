/*
	*  Code to send Heartbeat message to an MQTT topic.
	*  Heartbeat messages are those messages that are sent to the cloud to say that the device is alive.
	*  The messages will be sent in JSON format.
	*  Then we send it to AWS ioT core cloud through MQTT protocol.
	*  User can see this message by subscribing to a particular topic.
*/



#include <stdio.h> // for Standard Input and Output
#include <stdlib.h> // for functions involving memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays

#include <netdb.h> // for network database operations.
#include <ifaddrs.h> // for getting network interface addresses.

#include <shunyaInterfaces.h> // to program embedded boards to talk to PLC's, Sensors, Actuators and Cloud

#include <time.h> // to get and manipulate date and time information

#include "MQTTClient.h" // contains APIs to send/receive MQTT data



/* 
	*  Now fetching ID of the device.
	*  This data contains some parameters which will be stored in JSON format.
	*  First id of the device which is stored in "/etc/shunya/deviceid" will be fetched.
	*  Then UNIX timestamp fetched through API from time.h header file.
	*  After that type of event which will be "heartbeat" message.
	*  At the end we will fetch an IP address of the network at which our device is connected.
*/




void getdeviceid(void)  // Fetching device ID
{
FILE *idl;  // Interface and type library definitions
char ID[255];  // Variable definition
idl = fopen("/etc/shunya/deviceid", "r");  // Open a file to perform various operations 
while(fscanf(idl, "%s", ID)!=EOF)
{printf("%s ", ID );  // Print device ID
}


// Fetching timestamp

int TSP = (gettimeofday(2));  // Get and set the time as well as a timezone.




void checkIPbuffer(char *ip)  // Fetching IP address
{
if (NULL == ip)
{perror("inet_ntoa");  // Do some error checking 
exit(1);}}  // Convert IP string to dotted decimal format
char *ip;  // create pointer to ip
ip = inet_ntoa(*((struct in_addr*)  // Get value
host_entry->h_addr_list[0]));  // Convert into IP string
}


void AWS_configuration(void)  // configuring connection with AWS IoT core
{
"user": {
"endpoint": " ",  // Endpoint is the URL at which our network will connect.
"port": 8883,  // Port is default 8833
"certificate dir": "/home/shunya/.cert/aws/",  // Certificate directory is directory of the certificates of AWS
"root certificate": " ",  // oot certificate is a public key certificate
"client certificate": " ",  // lient certificate is used by client systems to make authenticated requests
"private key": " ",  // Private key is stored on user's device and is used to decrypt data.
"client ID": " "}  // Client ID is a special ID given to an user.
}


/*
	*  Next, sending a JSON message to an AWS ioT core MQTT broker.
	*  First creating new AWS instance, and connecting to that instance.
	*  Then creating a payload of JSON message.
	*  Using APIs payload it will be published to "device/heartbeat" topic.
	*  At the end connection should be released from MQTT broker for completing a process.
*/


void DataSend()  // sending a JSON message to an AWS ioT core MQTT broker.
{
MQTTClient_message heartbeat = MQTTClient_message_initializer; //initializes message to be sent on MQTT broker
awsObj user = newAws("user"); //creates new instance with AWS
awsConnectMqtt(&user); //connects to the MQTT broker of AWS ioT core
heartbeat.payload("device":{"deviceId": ID,"timestamp": TSP,"eventType": "heartbeat","ipAddress": ip}.getBytes()); //creates a payload of the message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat); //publishes a payload on AWS MQTT broker
awsDisconnectMqtt(&user); //releases connection with MQTT broker
}



/*
	*  In main calling all the functions.
	*  First calling shunyaInterfacesSetup function to initiate APIs contained in shunya interfaces library.
	*  Then fetching data of the device by calling getdeviceid function.
	*  In order to publish the data it is connected to MQTT broker.
	*  Finally by calling DataSend function publishing the data on particular topic.
*/


int main (void)  // In main calling all the functions
{
shunyaInterfacesSetup () ;// function to initiate shunya interfaces
getdeviceid(); // function to get data of the device (device id, timestamp, event type, IP address
AWS_configuration(); // function to configure connection with AWS MQTT broker
DataSend(); // function to publish data on AWS MQTT broker
}


