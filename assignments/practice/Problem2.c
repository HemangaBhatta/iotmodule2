/*
	*  Code for reading data from the RISHI Meter 3430 using Shunya Interfaces modbus API's.
	*  Current sensing, voltage sensing and power sensing units of the meter will give us data for required parameters.
	*  Fetching data for the required parameters from these units using an IoT Gateway and MODBUS (RS485) interface.
*/



#include <stdio.h> // for Standard Input and Output
#include <stdlib.h> // for functions involving memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays

#include <shunyaInterfaces.h> // to program embedded boards to talk to PLC's, Sensors, Actuators and Cloud

#include <dirent.h> // format of directory entries





rishimet = opendir("/dev/ttyAMA0");  // For Meter connections using RS485


/*
	*  First calling shunyaInterfacesSetup function to initiate APIs contained in shunya interfaces library.
	*  Then configuring MODBUS TCP/IP with modbus data.
	*  Creating modbus new instance and connecting as per configuration. 
	*  Finally reading all data of different parameters.
*/


int main(void)
{
shunyaInterfacesSetup();  //initialization of shunya interfaces

//modbus TCP/IP configuration


"modbus-tcp":{"type":"tcp","ipaddr":" ","port":" "};  //user needs to enter their modbus data

//modbus connect read/write data

modbusObj rishimet = newModbus("modbus-tcp"); // for creating new instance
modbusTcpConnect(&rishimet); // connect to modbus as per configuration

// reading data of different parameters

float ac_current = modbusTcpRead(&rishimet,30007);  // Fetching AC current data from Modbus address 30007
float voltage = modbusTcpRead(&rishimet, 30001);  // Fetching Voltage data from Modbus address 30001
float power = modbusTcpRead(&rishimet, 30013);  // Fetching Power data from Modbus address 30013
float Active_energy = modbusTcpRead(&rishimet, 30147);  // Fetching Active energy data from Modbus address 30147
float Reactive_energy = modbusTcpRead(&rishimet, 30151);  // Fetching Reactive energy data from Modbus address 30151
float Apparent_energy = modbusTcpRead(&rishimet, 30081);  // Fetching Apparent energy data from Modbus address 30081
float THD_of_Voltage = modbusTcpRead(&rishimet, 30219);  // Fetching THD of Voltage data from Modbus address 30219
    
return 0;  // Success
}


